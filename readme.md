Dynatag
=======

This web service makes it possible to create QR codes and NFC tags with dynamic links and information content.

**Note:** The current version is not ready and some of its functionalities are not implemented yet. The basic functionality, however, is there already. 

Installing and setting up
-------
This tool is made using Django web framework. If you are new to Django, take a look at [Django's documentation](https://docs.djangoproject.com/en/1.7/intro/tutorial01/) first to get a hunch of how it is used.

After you've done that, you should take a look at the file named `requirements.txt`. It contains a list of all the Python 2 libraries needed by this project.

There's also a file called `things_to_do_after_fork.txt` that contains a list of things you must do before starting to use this web application on your own server.

Copyright and license (GPL-3.0)
-------
Copyright (C) 2015 Mika Hämäläinen

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
