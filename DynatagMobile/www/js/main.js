var dynatagUrl = "http://127.0.0.1:8000/";

credents = ["",""];
var tagsList = null;

function login(username, password){
    credents=[username,password];
    login_view.style.display = "none";
    showLoading();
    $.post( dynatagUrl +"api_list/", { username: username, password: password } )  .done(function(data) {
    updateTagList(data);
    tagsList = data;
    saveCredentials(credents[0], credents[1]);
    credents = ["",""];
    hideLoading();
  })
  .fail(function() {
    alert( "Couldn't login!" );
    hideLoading();
    login_view.style.display = "block";
  });
}

function showLoading(){
    var $this = $( this ),
        theme = $this.jqmData( "theme" ) || $.mobile.loader.prototype.options.theme,
        msgText = $this.jqmData( "msgtext" ) || $.mobile.loader.prototype.options.text,
        textVisible = $this.jqmData( "textvisible" ) || $.mobile.loader.prototype.options.textVisible,
        textonly = !!$this.jqmData( "textonly" );
        html = $this.jqmData( "html" ) || "";
    $.mobile.loading( "show", {
            text: msgText,
            textVisible: textVisible,
            theme: theme,
            textonly: textonly,
            html: html
    });
}

function hideLoading(){
    $.mobile.loading( "hide" );
}

function updateTagList(data){
    console.log(data);

    tag_list = $( "#tags_ul" );
    tag_list.empty();
    for (tag_id in data){
        tag_data = data[tag_id];
        tag_html = "<li class='ui-li-static ui-body-inherit ui-first-child'><h2><a href='#' onclick=\"showTag('" + tag_id + "')\">" + tag_data.name + "</a></h2>";
        tag_html = tag_html + "<p>" + tag_data.description + "</p></li>";
        tag_list.append(tag_html);
        
    }
    
    tag_list_view.style.display = "block";
}

function saveCredentials(username, password){
    localStorage.setItem("username", username);
    localStorage.setItem("password", password);
}

function loginButton(){
    uname = username_field.value;
    psword = password_field.value;
    login(uname,psword);
}

function autoLogin(){
    if(localStorage.username){
        username_field.value = localStorage.username;
        password_field.value = localStorage.password;
        loginButton();
    }
}

function logout(){
    localStorage.removeItem("username");
    localStorage.removeItem("password");
    location.reload();
}

function showTag(tag){
    tagUrl = dynatagUrl + "redirect?tag=" + tag;
    qr.clear();
    qr.makeCode(tagUrl);
    showTagDialogTitle.textContent = tagsList[tag].name;
    showTagDialogEditButton.setAttribute('onclick',  'editTag("' + tag + '");');
    showTagDialogNFCButton.setAttribute('onclick',  'writeTag("' + tagUrl + '");');
    $( "#showTagDialog" ).popup( "open" );
    
}

function writeTag(url){
    var message = [
    ndef.uriRecord(url)];
    nfc.write(message, function (){
        toast = $("#toast");
        toast.text("NFC tag written!");
        toast.fadeIn(400).delay(3000).fadeOut(400);
    }, function(){
        toast = $("#toast");
        toast.text("Error writing NFC tag");
        toast.fadeIn(400).delay(3000).fadeOut(400);
    });
}

function editTag(tag){
    console.log(tag);
}