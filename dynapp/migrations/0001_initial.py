# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Tags',
            fields=[
                ('name', models.CharField(max_length=20)),
                ('description', models.CharField(max_length=200)),
                ('data', models.TextField()),
                ('tag_id', models.FloatField(serialize=False, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Types',
            fields=[
                ('name', models.CharField(max_length=10, serialize=False, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('username', models.CharField(max_length=20, serialize=False, primary_key=True)),
                ('password', models.CharField(max_length=500)),
                ('email', models.EmailField(max_length=75)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='tags',
            name='type',
            field=models.ForeignKey(to='dynapp.Types'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tags',
            name='user',
            field=models.ForeignKey(to='dynapp.Users'),
            preserve_default=True,
        ),
    ]
