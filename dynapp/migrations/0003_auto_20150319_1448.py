# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dynapp', '0002_auto_20150318_1641'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tags',
            name='type',
            field=models.CharField(max_length=20),
            preserve_default=True,
        ),
        migrations.DeleteModel(
            name='Types',
        ),
    ]
