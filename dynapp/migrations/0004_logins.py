# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dynapp', '0003_auto_20150319_1448'),
    ]

    operations = [
        migrations.CreateModel(
            name='Logins',
            fields=[
                ('username', models.CharField(max_length=20, serialize=False, primary_key=True)),
                ('count', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
