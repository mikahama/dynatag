# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dynapp', '0005_auto_20150908_1358'),
    ]

    operations = [
        migrations.CreateModel(
            name='Authorizations',
            fields=[
                ('email', models.CharField(unique=True, max_length=100)),
                ('key', models.CharField(max_length=200, serialize=False, primary_key=True)),
                ('expirity', models.DateTimeField()),
            ],
        ),
    ]
