# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dynapp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tags',
            name='tag_id',
        ),
        migrations.AddField(
            model_name='tags',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, default=1, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='types',
            name='content_type',
            field=models.CharField(default='text/html', max_length=10),
            preserve_default=False,
        ),
    ]
