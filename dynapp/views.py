from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.shortcuts import redirect
from dynapp.models import *
from datetime import datetime, timedelta
from django.contrib.auth import hashers
import qrcode
from django.http import Http404
import requests
from dynapp import secret_keys
import json
import urllib
from django.core.mail import send_mail
from django.utils import timezone
import string
from random import choice
# Create your views here.
tag_url = 'http://www.xn--mi-wia.com/dynatag/redirect?tag='
login_url = 'https://www.xn--mi-wia.com/dynatag/renewpassword/'


def index(request):

    user_name = request.session.get('user_name', None)
    if user_name != None:
        return redirect('main')

    app_name = "Dynatag"
    template = loader.get_template('index.html')
    auth_fail = ""
    create_fail = request.session.get('create_fail', "")
    request.session['create_fail'] = "" #Clear message for future requests
    if request.session.get('auth_fail', False):
        auth_fail = "Wrong username and password"
        request.session['auth_fail'] = False #Clear state for future requests
    context = RequestContext(request, {
        'app_name': app_name,
        "auth_fail": auth_fail,
        "create_fail": create_fail,
    })
    return HttpResponse(template.render(context))

def check_credentials(user, password):
    try:
        l = Logins.objects.get(username=user)
        if l.count > 100:
            #too many failed logins
            return 1
    except:
        l = Logins(username=user, count=0)
        l.save()
    l.count = l.count +1
    l.save()
    try:
        loguser = Users.objects.get(username=user)
        if hashers.check_password(password, loguser.password):
            #All good, login!
            l.count = 0
            l.save()
            return 0
        else:
            #wrong password
            return 2
    except:
        #Wrong username
        return 2

#login request
def login(request):
    if request.method == "POST":
        user = request.POST.get("username")[:20]
        password = request.POST.get("password")[:20]

        status = check_credentials(user, password)
        if status == 1:
            #too many failed logins
            request.session["captcha_username"] = user
            request.session["captcha_error"] = ""
            return redirect('captcha')
        if status == 0:
            request.session.flush()
            request.session['user_name'] = user
            return redirect('main')
        else:
            request.session['auth_fail'] = True
            return redirect('index')
    else:
        #method is not post -> redirect to index
        return redirect('index')


#create new user request
def create_account(request):
    if request.method == "POST":
        user = request.POST.get("username")[:20]
        password = request.POST.get("password")[:20]
        email = request.POST.get("email")

        if len(user)*len(password)*len(email) == 0 or "@" not in email:
            #Not accepted data in some of the fields
            request.session['create_fail'] = "All fields must be filled properly"
            return redirect('index')
        try:
            #User already exists
            loguser = Users.objects.get(username=user)
            request.session['create_fail'] = "Username is already in use"
            return redirect('index')
        except:
            #Create a new user
            request.session.flush()
            encoded_password = hashers.make_password(password)
            u = Users(username=user, password=encoded_password, email=email)
            u.save()
            request.session['user_name'] = user
            return redirect('main')
    else:
        #method is not post -> redirect to index
        return redirect('index')

#Tag list UI
def list_tags(request):
    user_name = request.session.get('user_name', None)
    global tag_url

    if user_name is None:
        #The user is not logged in!
        return redirect('index')

    user_object = Users.objects.get(username=user_name)

    template = loader.get_template('my_tags.html')
    tags = Tags.objects.filter(user=user_object).order_by("name")
    context = RequestContext(request, {
        "user_name": user_name,
        "tags": tags,
        "qr_url": tag_url,
    })
    return HttpResponse(template.render(context))

#Add tag UI
def add_tag(request):
    user_name = request.session.get('user_name', None)

    if user_name is None:
        #The user is not logged in!
        return redirect('index')
    template = loader.get_template('add_tag.html')
    context = RequestContext(request, {
        "user_name": user_name,
        "tag_id": "",
        "tag_data": "",
        "tag_type": "",
        "tag_name": "",
        "tag_desc": "",
    })
    return HttpResponse(template.render(context))

#Add a new tag and redirect
def new_tag(request):
    user_name = request.session.get('user_name', None)
    if request.method != "POST" or user_name is None:
        #Wrong method (GET) or user not logged in
        return redirect('index')
    tag_name = request.POST.get("name")[:20]
    if len(tag_name) == 0:
        tag_name = "Noname"
    t_type = request.POST.get("tag_mimetype")

    user_object = Users.objects.get(username=user_name)

    desc = request.POST.get("description")[:200]
    tag_data = request.POST.get("tag_data")[:700]
    t = Tags(user=user_object,type=t_type,name=tag_name,description=desc,data=tag_data)
    t.save()
    request.session['new_tag_id'] = str(t.id)
    return redirect('tag_added')

#Get QR code
def get_qr(request):
    tag = request.GET.get("tag")
    response = HttpResponse(content_type="image/png")
    global tag_url
    img = qrcode.make(tag_url +tag)
    img.save(response, "PNG")
    return response

#page that shows that a tag has been tag_added
def tag_added(request):
    user_name = request.session.get('user_name', None)
    new_tag_uid = request.session.get('new_tag_id', None)
    if user_name is None or new_tag_uid is None:
        #The user is not logged in or a tag hasn't been created
        return redirect('index')

    new_tag_id = str(new_tag_uid)
    qr_url = "../../../qr/?tag=" + new_tag_id
    global tag_url
    current_tag_url = tag_url +new_tag_id

    template = loader.get_template('tag_added.html')
    context = RequestContext(request, {
        "tag_url": current_tag_url,
        "qr_url": qr_url,
    })
    return HttpResponse(template.render(context))

#page that redirects to a tag
def redirect_tag(request):
    tag = request.GET.get("tag")
    if tag is None:
        raise Http404("No tag specified.")
    try:
        tag_object = Tags.objects.get(id=tag)
        if tag_object.type != "url":
            response = HttpResponse(tag_object.data, content_type=tag_object.type)
            return response
        else:
            template = loader.get_template('redirect.html')
            context = RequestContext(request, {
                "url": tag_object.data,
            })
            return HttpResponse(template.render(context))
    except:
        raise Http404("No tag matches the given query.")

#logs the user out
def logout(request):
    request.session.flush()
    return redirect('index')

def edit_tag(request):
    tag = request.GET.get("tag",None)
    user_name = request.session.get('user_name', None)
    if user_name is None:
        #The user is not logged in!
        return redirect('index')
    elif tag is None:
        #no tag id is provided
        return redirect('main')
    try:
        tag_object = Tags.objects.get(id=tag)
        user_object = Users.objects.get(username=user_name)
        if tag_object.user == user_object:
            #The user owns the tag and thus eiditing is allowed
            template = loader.get_template('add_tag.html')
            context = RequestContext(request, {
                "user_name": user_name,
                "tag_id": tag,
                "tag_data": tag_object.data,
                "tag_type": tag_object.type,
                "tag_name": tag_object.name,
                "tag_desc": tag_object.description,
            })
            return HttpResponse(template.render(context))
        else:
            #User doesn't own the tag -> redirect away
            return redirect('main')
    except:
        #No tag with the specific id -> redirection
        return redirect('main')


def settings(request):
    user_name = request.session.get('user_name', None)
    error = request.session.get('settings_fail', "")
    request.session['settings_fail'] = ""
    if user_name is None:
        #The user is not logged in!
        return redirect('index')
    user_object = Users.objects.get(username=user_name)
    template = loader.get_template('settings.html')
    context = RequestContext(request, {
        "email": user_object.email,
        "error": error,
    })
    return HttpResponse(template.render(context))

def show_captcha(request):
    template = loader.get_template('captcha.html')
    captcha_error = request.session.get("captcha_error", "")
    context = RequestContext(request, {
        "public_key": secret_keys.recaptcha_public(),
        "captcha_error": captcha_error,
    })
    request.session["captcha_error"] = ""
    return HttpResponse(template.render(context))

def check_captcha(request):
    response = request.POST.get("g-recaptcha-response","")
    payload = {'secret': secret_keys.recaptcha_secret(), 'response': response}
    r = requests.post("https://www.google.com/recaptcha/api/siteverify", params=payload)
    r_json = r.json()
    if r_json["success"] == True:
        #captcha ok -> set login count to 0
        user = request.session.get("captcha_username", "")
        try:
            l = Logins.objects.get(username=user)
            l.count = 0
            l.save()
        except:
            pass
        return redirect('index')
    else:
        #Incorrect captcha
        request.session["captcha_error"] = "Incorrect captcha!"
        return redirect('captcha')



def save_settings(request):
    user_name = request.session.get('user_name', None)
    if request.method != "POST" or user_name is None:
        #Wrong method (GET) or user not logged in
        return redirect('index')
    user_object = Users.objects.get(username=user_name)
    password = request.POST.get("oldpass")[:20]
    if hashers.check_password(password, user_object.password) ==False:
        request.session['settings_fail'] = "Wrong password"
        return redirect('settings')
    email = request.POST.get("email")
    newpass1 = request.POST.get("newpass1")[:20]
    newpass2 = request.POST.get("newpass2")[:20]
    if "@" not in email:
        request.session['settings_fail'] = "Please enter a correct e-mail address"
        return redirect('settings')
    if len(newpass1)*len(newpass2) != 0:
        #Change password
        if newpass1 != newpass2:
            request.session['settings_fail'] = "New passwords don't match"
            return redirect('settings')
        user_object.password = hashers.make_password(newpass1)
    user_object.email = email
    user_object.save()
    return redirect('main')

#Add a new tag and redirect
def save_tag(request):
    user_name = request.session.get('user_name', None)
    if request.method != "POST" or user_name is None:
        #Wrong method (GET) or user not logged in
        return redirect('index')
    tag_name = request.POST.get("name")[:20]
    if len(tag_name) == 0:
        tag_name = "Noname"
    t_type = request.POST.get("tag_mimetype")

    try:
        tag_id = request.POST.get("edit_tag_id")

        user_object = Users.objects.get(username=user_name)
        tag_object = Tags.objects.get(id=tag_id)
        if tag_object.user != user_object:
            raise Http404("The current user doesn't have the requested tag")

        desc = request.POST.get("description")[:200]
        tag_data = request.POST.get("tag_data")[:700]
        tag_object.type = t_type
        tag_object.name = tag_name
        tag_object.description = desc
        tag_object.data = tag_data
        tag_object.save()
        request.session['new_tag_id'] = str(tag_id)
        return redirect('tag_added')
    except:
        raise Http404("Unknown tag id")

def generate_secure_key():
    return str(uuid.uuid4())

def remove_authorization(email):
	try:
		auth = Authorizations.objects.get(email=email)
		auth.delete()
	except:
		pass

def request_new_password(request):
    email = request.GET.get("email")
    uname = request.GET.get("username")
    try:
        Users.objects.get(username=uname, email=email)
        #User exists
        return send_key_to(request, email, uname)
    except:
        return HttpResponse(status=400)


def send_key_to(request, email,uname):
	if email is None or "@" not in email:
		return HttpResponse(status=400)
	expirity = datetime.now() + timedelta(days=1)
	key_auth = generate_secure_key()
	try:
		auth = Authorizations.objects.get(email=uname)
		if auth.expirity > timezone.now():
			#There is a token and it's not expired -> don't send a new one
			return HttpResponse(status=409)
		else:
			#Let's update the expired token
			remove_authorization(uname)
	except:
		#There is no token registered for the email
		pass
	auth = Authorizations(email=uname, key=key_auth, expirity=expirity)
	auth.save()

	global login_url

	link = login_url + "?token=" + urllib.quote_plus(key_auth) + "&username=" + urllib.quote_plus(uname)
	title = "Dynatag, password request"
	body= u"Hello,\n\nYou have requested a new password to Dynatag. You can reset your password by following this link:\n\n"
	body = body + link + u"\n\nThis link will be valid for 24 hours.\n\nThis is an automatically generated response. Don't answer to this email directly."

	send_mail(title, body, 'noreply@xn--mi-wia.com', [email], fail_silently=False)
	return HttpResponse(status=200)

def renew_password(request):
	key = request.GET.get("token", "")
	uname = request.GET.get("username", "")
	try:
		auth = Authorizations.objects.get(key=key,email=uname)
		user_object = Users.objects.get(username=uname)
		if auth.expirity > timezone.now():
			#There is a token and it's not expired -> login
			new_password = ''.join(choice(string.letters + string.digits) for _ in range(8))
			user_object.password = hashers.make_password(new_password)
			auth.delete()
			user_object.save()
			message = "Password was changed for " + uname + ". The new password is " + new_password
		else:
			message = "The password request had already expired."
	except:
		message = "Unknown request."
	template = loader.get_template('new_password.html')
	context = RequestContext(request, {
		"message": message,
	})
	return HttpResponse(template.render(context))

@csrf_exempt
def api_list_tags(request):
    if request.method != "POST":
        return HttpResponse(status=405)

    user_name = request.POST.get("username","")
    password = request.POST.get("password","")
    if check_credentials(user_name,password) != 0:
        return HttpResponse(status=401)


    user_object = Users.objects.get(username=user_name)
    tags = Tags.objects.filter(user=user_object).order_by("name")
    tags_json = {}
    for tag in tags:
        tag_json = {"name": tag.name, "description": tag.description, "type":tag.type, "data":tag.data}
        tags_json[str(tag.id)] = tag_json
    return HttpResponse(json.dumps(tags_json), content_type="application/json", status=200)


@csrf_exempt
def api_set_tag(request):
    if request.method != "POST":
        return HttpResponse(status=405)

    user_name = request.POST.get("username","")
    password = request.POST.get("password","")
    tag_id = request.POST.get("tag_id",None)

    desc = request.POST.get("description")[:200]
    tag_data = request.POST.get("tag_data")[:700]
    tag_name = request.POST.get("name")[:20]
    if len(tag_name) == 0:
        tag_name = "Noname"
    t_type = request.POST.get("tag_mimetype")


    if check_credentials(user_name,password) != 0:
        return HttpResponse(status=401)

    user_object = Users.objects.get(username=user_name)
    try:
        if tag_id is not None:
            tag_object = Tags.objects.get(id=tag_id)
            if tag_object.user != user_object:
                return HttpResponse(status=401)
        else:
            tag_object = Tags(user=user_object)
        tag_object.type = t_type
        tag_object.name = tag_name
        tag_object.description = desc
        tag_object.data = tag_data
        tag_object.save()
        tag_id = str(tag_object.id)
    except:
        return HttpResponse(status=500)

    message = {"tag_id" : tag_id}
    return HttpResponse(json.dumps(message), content_type="application/json", status=200)