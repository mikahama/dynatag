from django.db import models
import uuid

# Create your models here.
class Users(models.Model):
    username = models.CharField(max_length=20, primary_key=True)
    password = models.CharField(max_length=500)
    email = models.EmailField()


class Tags(models.Model):
    user = models.ForeignKey(Users)
    type = models.CharField(max_length=20)
    name = models.CharField(max_length=20)
    description = models.CharField(max_length=200)
    data = models.TextField()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

class Logins(models.Model):
    username = models.CharField(max_length=20, primary_key=True)
    count = models.IntegerField(default=0)

class Authorizations(models.Model):
	email = models.CharField(max_length=100, unique=True)
	key = models.CharField(max_length=200, primary_key=True)
	expirity = models.DateTimeField()