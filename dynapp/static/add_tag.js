function initView(){
	if(old_tag_data.textContent == ""){
		//Add view
		url_radio.checked = true;
		showSettings();
	}else{
		//Edit view
		heading.textContent = "Edit Tag";
		tag_name.value = old_tag_name.textContent;
		tag_desc.value = old_tag_description.textContent;
		var type = old_tag_type.textContent;
		var data = old_tag_data.textContent;
		if(type == "url"){
			if (data.indexOf("tel:") == 0){
				phone_radio.checked = true;
				phone_input.value = data.substring(4);
			}else if (data.indexOf("mailto:") == 0){
				email_radio.checked = true;
				email_input.value = data.substring(7);
			}else{
				url_radio.checked = true;
				url_input.value = data;
			}
		} else if(type == "text/vcard"){
			parseVCard(data);
			vcard_radio.checked = true;
		} else if(type == "text/calendar"){
			parseICal(data);
			ical_radio.checked = true;
		}
		submit_button.textContent = "Save"
		showSettings();
	}
}

function showSettings(){
	var settings = document.getElementsByClassName("tag_settings");
	for (var i = 0; i < settings.length; i++) {
		settings[i].style.display = "none";
	}
	document.getElementById(getSelectedRadioButtonValue()).style.display = "block";
}

function getSelectedRadioButtonValue(){
	var radiobuttons = document.getElementsByClassName("radiobutton");
	for (var i = 0; i < radiobuttons.length; i++) {
		if(radiobuttons[i].checked){
			return radiobuttons[i].value;
		}
	}
}

function createVCard(){
	var vcard = "BEGIN:VCARD\nVERSION:4.0";
	vcard = vcard + "\nN:" + vcard_lname_input.value +";" +vcard_fname_input.value+";;;";
	vcard = vcard + "\nTEL;TYPE=home,voice;VALUE=uri:tel:" + vcard_phone_input.value;
	vcard = vcard + "\nEMAIL:"+vcard_email_input.value;
	vcard = vcard + "\nADR;TYPE=work;LABEL=\"" +vcard_address_input.value+"\n"+ vcard_city_input.value +", "+ vcard_zip_input.value +"\n"+vcard_country_input.value+"\":;;" +vcard_address_input.value+";"+ vcard_city_input.value +";;"+ vcard_zip_input.value +";"+vcard_country_input.value;
	vcard = vcard + "\nEND:VCARD";
	return vcard;
}

function parseVCard(vcard){
	var lines = vcard.split("\n");
	var nextIsAddress = false;
	for (var i = 0; i < lines.length; i++) {
		var line = lines[i];
		if(line.indexOf("N:") == 0){
			//Name
			line = line.substring(2);
			var names = line.split(";");
			vcard_lname_input.value = names[0];
			vcard_fname_input.value = names[1];
		}else if(line.indexOf("TEL;") == 0){
			var numberStart = line.indexOf("VALUE=uri:tel:");
			vcard_phone_input.value = line.substring(numberStart +14);
		}else if(line.indexOf("EMAIL:") == 0){
			vcard_email_input.value = line.substring(6);
		}else if (line.indexOf("ADR;") == 0){
			nextIsAddress = true;
		}
		else if(nextIsAddress && line.indexOf("\":;;") != -1){
			var dataLine = line.split(":");
			var address_data = dataLine[1].split(";");
			vcard_address_input.value = address_data[2];
			vcard_city_input.value = address_data[3];
			vcard_zip_input.value = address_data[5];
			vcard_country_input.value = address_data[6];
			nextIsAddress = false;
		}
	}

}

function createICal(){
	var ical = "BEGIN:VCALENDAR\nVERSION:2.0\nBEGIN:VEVENT";
	ical = ical +"\nSUMMARY:"+cal_title_input.value;
	ical = ical +"\nDTSTART:"+parseDateForPost(cal_start_input.value);
	ical = ical +"\nDTEND:"+parseDateForPost(cal_end_input.value);
	ical = ical +"\nLOCATION:"+cal_place_input.value;
	ical = ical +"\nEND:VEVENT\nEND:VCALENDAR";
	return ical;
}

function parseICal(data){
	var lines = data.split("\n");
	for (var i = lines.length - 1; i >= 0; i--) {
		line = lines[i];
		if(line.indexOf("SUMMARY:") == 0){
			cal_title_input.value = line.substring(8);
		} else if(line.indexOf("LOCATION:") == 0){
			cal_place_input.value = line.substring(8);
		} else if(line.indexOf("DTSTART:") == 0){
			cal_start_input.value = parseDateForInput(line.substring(8));
		} else if(line.indexOf("DTEND:") == 0){
			cal_end_input.value = parseDateForInput(line.substring(6));
		}
	}
}

function parseDateForInput(date){
	//19980118T230000
	var return_date = date.substring(0,4)+"-"+ date.substring(4,6) +"-" +date.substring(6,8) +"T" + date.substring(9,11)+":" +date.substring(11,13)+ ":"+ date.substring(13,15) + "Z";
	return return_date;
}

function parseDateForPost(date){
	var return_date = replaceAll(date, "-", "");
	return_date = replaceAll(return_date, ":", "") + "00";
	return return_date;
}

function replaceAll(string, find, replace) {
  return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function addTag(){
	if(tag_name.value.length * tag_desc.value.length == 0){
		err_message.textContent = "Please provide a name and a description.";
		return;
	}
	var type = getSelectedRadioButtonValue();
	var data ="";
	var mime = "url";
	if (type == "url"){
		data = url_input.value;
		if(data.indexOf(":")==-1){
			//There is no :, so let's add at least http:
			data = "http://" + data;
		}
	}else if (type == "email"){
		data = "mailto:" + email_input.value;
	}else if (type == "phone"){
		data = "tel:" + phone_input.value;
	}else if (type == "vcard"){
		data = createVCard();
		mime = "text/vcard";
	}else if (type == "cal"){
		if(cal_start_input.value == ""|| cal_end_input.value == ""){
			err_message_date.textContent = "Please enter dates and times";
			return;
		}
		data = createICal();
		mime = "text/calendar";
	}

	form_tag_name.value = tag_name.value;
	form_tag_desc.value = tag_desc.value;
	form_tag_data.value = data;
	form_tag_type.value = mime;
	post_form.submit();
}

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}