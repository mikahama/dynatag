from django.conf.urls import patterns, url

from dynapp import views

urlpatterns = patterns('',
    # ex: /polls/
    url(r'^$', views.index, name='index'),
    url(r'login/', views.login, name='login'),
    url(r'create_account/', views.create_account, name='create_account'),
    url(r'my_tags/add/save/', views.new_tag, name='new_tag'),
    url(r'my_tags/add/success/', views.tag_added, name='tag_added'),
    url(r'my_tags/add/', views.add_tag, name='add_tag'),
    url(r'my_tags/save/', views.save_tag, name='save_tag'),
    url(r'my_tags/edit', views.edit_tag, name='edit_tag'),
    url(r'my_tags/', views.list_tags, name='main'),
    url(r'qr/', views.get_qr, name='qr_image'),
    url(r'redirect', views.redirect_tag, name='redirect'),
    url(r'logout/', views.logout, name='logout'),
    url(r'settings/save/', views.save_settings, name='save_settings'),
    url(r'settings/', views.settings, name='settings'),
    url(r'change_password/', views.request_new_password, name='password_change'),
    url(r'renewpassword/', views.renew_password, name='password_renewal'),
    url(r'human_verification/', views.show_captcha, name='captcha'),
    url(r'captcha_check/', views.check_captcha, name='check_captcha'),
    url(r'api_list/', views.api_list_tags, name='api_list'),
    url(r'api_save_tag/', views.api_set_tag, name='api_set_tag'),
)